#-*- mode: shell-script -*-
# Make tramp happy by setting a simple prompt and returning early
[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ ' && return
HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=1000

export TERM="xterm-256color"
setopt auto_cd
setopt extended_glob

# Allow redirecting I/O to multiple files
setopt multios

# Ignore duplicates in history
setopt hist_ignore_all_dups

# write every new command to history before it is executed
setopt inc_append_history

# Reread history file every time history is needed
setopt share_history

# Reduce sequences of spaces to one space in history
setopt hist_reduce_blanks

# Save time and elapsed time of commands in history
setopt extended_history

# Confirm execution of commands when using bang history
setopt hist_verify

# Enable command and variable substitutions in the prompt
setopt prompt_subst

# If ~/.zsh/functions exists, add it to fpath
if [[ -d ~/.zsh/functions ]]; then
    fpath=(~/.zsh/functions $fpath)
fi

# Emacs style keybindings
bindkey -e

autoload -Uz compinit
compinit

# Some completion style settings (copied from default ubuntu configuration)
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

#Remove '/' from WORDCHARS, so we can use C-w to erase path components
WORDCHARS="${WORDCHARS:s#/#}"

#Setup associative arrays with colors for use in prompt definition
autoload -Uz colors
colors

autoload -Uz vcs_info

zstyle ':vcs_info:*' enable git hg cvs svn p4 tla bzr
zstyle ':vcs_info:*' patch-format ' [%p] (%n/%a)'

#Define environment variables in ~/.zsh env
if [[ -r ~/.zshenv ]]; then
    source ~/.zshenv
fi

# Define all aliases in ~/.aliases
if [[ -r ~/.aliases ]]; then
    source ~/.aliases
fi

# Customize these variables in .zshrc-local to add custom segments to the prompt
PL9K_EXTRA_RIGHT_PROMPT_ELEMENTS=()
PL9K_EXTRA_LEFT_PROMPT_ELEMENTS=()

# Run any local configurations if they exist
if [[ -r ~/.zshrc-local ]]; then
    source ~/.zshrc-local
fi

# Use the PL9K theme
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(${PL9K_EXTRA_LEFT_PROMPT_ELEMENTS} time context dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs ${PL9K_EXTRA_RIGHT_PROMPT_ELEMENTS})
POWERLEVEL9K_TIME_FORMAT="%D{%H:%M}"
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
POWERLEVEL9K_DIR_SHOW_WRITABLE=true
DEFAULT_USER=goedson
source  ~/.zsh/pl9k/powerlevel9k.zsh-theme
