#-*- mode:shell-script -*-
# Make emacsclient the default editor
export EDITOR='emacsclient -c'

# Make emacsclient spawn a new emacs daemon if it can't connect to one
export ALTERNATE_EDITOR=''


if [[ -r ~/.zshenv-local ]]; then
    source ~/.zshenv-local
fi

path=(~/bin ~/scripts $path)

export DEBEMAIL="goedson@debian.org"
export DEBFULLNAME="Goedson Teixeira Paixao"
